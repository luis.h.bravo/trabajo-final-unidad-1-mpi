/*
Autor: Luis Bravo
Fecha: 16-05-2022
 */
package Controlador;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

/**
 *
 * @author bravotoledo
 */
public class MPI {
    String ruta;
    int nprocesadores;
    
     public void numeroProcesadores(){
        nprocesadores = Runtime.getRuntime().availableProcessors()/2;
     }

     public void ejecutar(int np, JTextArea areaTx){
        try {
            Process ejecutar;
            String presentar ="";
            String run = "mpirun -np "+ np +" a.out";
            ejecutar = Runtime.getRuntime().exec(run);
            BufferedReader buff = new BufferedReader(new InputStreamReader(ejecutar.getInputStream()));
            String aux;
            while ((aux = buff.readLine()) != null) {
                presentar += aux +"\n";
            }
            areaTx.setText(presentar);
        } catch (IOException ex) {
            Logger.getLogger(MPI.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
     
     public boolean esNUmero(String cadena){
         	try {
		Integer.parseInt(cadena);
		return true;
	} catch (NumberFormatException nfe){
		return false;
	}
     }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public int getNprocesadores() {
        return nprocesadores;
    }

    public void setNprocesadores(int nprocesadores) {
        this.nprocesadores = nprocesadores;
    }
     
     public boolean validarExtencion(){
        String fileName = ruta;
        String fileExt = "";
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            fileExt = fileName.substring(i+1);
        }
        return fileExt.equalsIgnoreCase("c");
     }
     public void compilarFile(String name){
        try {
            Process compilar;
            compilar = Runtime.getRuntime().exec("mpicc "+name);
        } catch (IOException ex) {
            Logger.getLogger(MPI.class.getName()).log(Level.SEVERE, null, ex);
        }
            
     }
    
}
